import React from 'react'
import { Col, Row, Image, Button, Menu, Dropdown, Space } from 'antd'
import c from "./navigation.module.scss";

export const Navigation = () => {

    const _renderHamburgerMenu = (
        <Menu style={{ minWidth: '150px' }}>
            <Menu.Item key="1">
                <Button type={'text'} href='#products' >
                    Products
                </Button>
            </Menu.Item>
            <Menu.Item key="2">
                <Button type={'text'} href='#values' >
                    Why us?
                </Button>
            </Menu.Item>
            <Menu.Item key="3">
                <Button type={'text'} href='#contact' >
                    Contact
                </Button>
            </Menu.Item>
        </Menu>
    );

    return (
        <div className={c.navbar} >
            <Row align={'middle'} justify={'space-between'} style={{ maxWidth: '1200px', margin: '0 auto' }}>
                <Col xs={12} md={{ span: 4, offset: 0 }}>
                    <Image
                        src={'/images/logo.svg'}
                        alt={'arvis logo'}
                        width={125}
                        preview={false}
                        onClick={() => window.location.reload()}
                    />
                </Col>
                <Col xs={{ span: 4, offset: 8 }} md={{ span: 0, offset: 0 }}>
                    <Dropdown placement={'bottomLeft'} overlay={_renderHamburgerMenu} >
                        <Button
                            type={'text'}
                            shape={'round'}
                            className="ant-dropdown-link header_margin"
                            onClick={e => e.preventDefault()}
                        >
                            <Image
                                src={'/images/humberger.svg'}
                                alt={'arvis logo'}
                                width={25}
                                preview={false}
                            />
                        </Button>
                    </Dropdown>
                </Col>
                <Col xs={0} sm={0} md={{ span: 12, offset: 0 }} className={c.menuWrap} >
                    <Space direction={'horizontal'} size={'large'} style={{ paddingRight: '25px' }}>
                        <Button type={'text'} size={'large'} href={'#hero'} className={c.menuBar}>
                            Home
                        </Button>
                        <Button type={'text'} size={'large'} href={'#products'} className={c.menuBar}>
                            Products
                        </Button>
                        <Button type={'text'} size={'large'} href={'#values'} className={c.menuBar}>
                            Why us?
                        </Button>
                        <Button type={'text'} size={'large'} href={'#contact'} className={c.menuBar}>
                            Contact
                        </Button>
                    </Space>
                </Col>
            </Row>
        </div>
    )
}
