import React from 'react'
import { Image, Col, Row, Typography } from 'antd'
import c from './values.module.scss'

const { Text, Title } = Typography;

export interface IListValue {
    image: string;
    title: string;
    desc: string;
}

export default function Values() {
    const listValue: IListValue[] = [
        {
            image: '/images/employees1.svg',
            title: 'Employees',
            desc: 'Efficient workflow allows employees to focus on what matters, take ownership of their work.'
        },
        {
            image: '/images/middle1.svg',
            title: 'Middle Management',
            desc: 'Measurable Metrics, Transparent unit health and performance analysis and reviews. Control and collaborate with Team Members for Unit Growth.'
        },
        {
            image: '/images/directors3.svg',
            title: 'Directors & C-Levels',
            desc: 'Powerful actionable data and Business Intelligence, extend your reach, be in full control. Make powerful decisions backed by Objective, Quantified Data.'
        },
        {
            image: '/images/shareholders1.svg',
            title: 'Shareholders',
            desc: 'Increased Upside through Opex mitigation, Increased Returns through Process Efficiency. Unlock the Upside.'
        },
    ]

    const _renderValues = listValue.map((item) =>

        <Col xs={24} md={12} style={{ display: 'grid', padding: '30px 40px' }}>
            <div style={{ display: "flex" }}>
                <div style={{ marginRight: "18px" }}>
                    <Image
                        preview={false}
                        src={item.image}
                        width={100}
                        style={{ marginBottom: '18px' }}
                    />
                </div>
                <div>
                    <Title level={5}>{item.title}</Title>
                    <Text>{item.desc}</Text>
                </div>
            </div>
        </Col>
    )

    return (
        <div id={'values'} className={c.valuesWrap}>
            <Row align={'middle'} justify={'center'} style={{ maxWidth: '1200px' }}>
                <Col xs={{ span: 18, offset: 0 }} md={{ span: 7, offset: 1 }} style={{ padding: '40px 0 0 0', textAlign: "center" }}>
                    <Row align={'top'} style={{ textAlign: 'center' }}>
                        <Image
                            src={'/images/empowers.svg'}
                            width={300}
                            preview={false}
                        />
                        <Title level={3} style={{ margin: '20px' }}>
                            How ARVIS Empowers Businesses
                        </Title>
                    </Row>
                </Col>
                <Col xs={24} md={15}>
                    <Row gutter={20}>
                        {_renderValues}
                    </Row>
                </Col>
            </Row>
        </div>
    )
}
