import React from 'react'
import { Button, Col, Image, Row, Typography } from 'antd'
import c from './hero.module.scss'

const { Text, Title } = Typography;

export default function Hero() {
    return (
        <div id={'hero'} className={c.heroWrap}>
            <Row gutter={32} align={'middle'} style={{ maxWidth: '1200px' }}>
                <Col xs={24} sm={12} style={{ display: 'grid' }}>
                    <Title level={2}>
                        Solutions that are tailor engineered to simplify,
                        automate and streamline your business process
                    </Title>
                    <Text>
                        Our add-on solutions enhance the user experience,
                        increasing productivity by providing greater ease of use through simplified and
                        intuitive user interfaces tailored to your enterprise.
                    </Text>
                    <Button
                        style={{
                            marginTop: '20px',
                            width: '200px',
                        }}
                        type={'primary'}
                        size={'large'}
                        href={'#products'}
                    >
                        <b>Our Services</b>
                    </Button>
                </Col>
                <Col xs={0} sm={12} >
                    <Row justify={'center'} >
                        <Image
                            src={'/images/undraw_revenue_3osh.svg'}
                            width={500}
                            preview={false}
                        />
                    </Row>
                </Col>
            </Row>
        </div>
    )
}
