import React from 'react';
import { Image, Card, Col, Row, Typography } from 'antd';
import c from './products.module.scss';

const { Text, Title } = Typography;

export interface IListProduct {
    image: string;
    title: string;
    desc: string;
}

export default function Products() {
    const listProduct: IListProduct[] = [
        {
            image: '/images/undraw_Add_files_re_v09g.svg',
            title: 'Smart Doc',
            desc: 'Create documents & contracts in minutes, Create /any/ template to automate and add to your business process, Certified Digital Sign'
        },
        {
            image: '/images/undraw_data_report_bi6l.svg',
            title: 'Management Analytics',
            desc: 'Keep track of what matters; Gather and collect data from your Digital Workflow, track Revenue Leaders and Loss Leaders, etc.'
        },
        {
            image: '/images/undraw_secure_data_0rwp.svg',
            title: 'Supporting Service',
            desc: 'Validate your financial documents digitally with E-Meterais, Collaborate and work together cross Divisions, complete with an audit process; and DARCI powered, ensuring accountability in every step of the way'
        },
    ]

    const _renderProducts = listProduct.map((item) =>
        <Col xs={24} md={8} className={c.productsCard}>
            <Card
                style={{
                    textAlign: 'center',
                    minHeight: '400px',
                    padding: '20px 20px',
                }}
            >
                <Image
                    src={item.image}
                    height={160}
                    style={{ marginBottom: '25px' }}
                    preview={false}
                />
                <Title level={5}>{item.title}</Title>
                <Text>{item.desc}</Text>
            </Card>
        </Col>
    )

    return (
        <div id={'products'} className={c.productsWrap}>
            <Row align={'middle'} justify={'center'} style={{ maxWidth: '1200px' }}>
                <Col xs={24} md={24} style={{ textAlign: 'center', marginBottom: '15px' }}>
                    <Text type={'secondary'}>SERVICES</Text>
                    <Title level={2} style={{ margin: '0' }}>
                        Data Analytics & Smart ERP Service
                    </Title>
                </Col>
                <Col xs={24} md={24}>
                    <Row justify={'center'} align={'middle'}>
                        {_renderProducts}
                    </Row>
                </Col>
            </Row>
        </div>
    )
}
