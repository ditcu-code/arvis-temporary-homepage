import React from 'react'
import { Col, Row, Typography, Image, Button, Space } from 'antd'
import c from './footer.module.scss'

const { Text, Title } = Typography;

export default function Footer() {

    function _renderContactFooter() {
        return (
            <div className={c.contactFooter}>
                <Row align={'middle'} justify={'space-between'} style={{ padding: '25px 0', maxWidth: '1200px' }}>
                    <Col xs={24} md={20}>
                        <Title level={2} className={c.contactText}>
                            Find out why companies are signing up for Arvis
                        </Title>
                    </Col>
                    <Col xs={24} md={{ span: 2, offset: 2 }}>
                        <Row justify={'center'} className={c.contactButton}>
                            <Button
                                type={'primary'}
                                style={{ height: '45px', minWidth: '150px' }}
                                onClick={() => window.open("https://fwlibjm9qo6.typeform.com/to/SaHvx1RO", "_blank")}
                            >
                                <b>Contact Us</b>
                            </Button>
                        </Row>
                    </Col>
                </Row>
            </div>
        )
    }

    function _renderMainFooter() {
        return (
            <div className={c.centerFooter}>
                <Row align={'top'} justify={'center'} style={{ maxWidth: '1200px' }}>
                    <Col xs={24} md={12} className={c.arvis}>
                        <Image
                            src={'/images/logo.svg'}
                            style={{ filter: 'invert(100%)', marginBottom: '25px' }}
                            width={250}
                            preview={false}
                        />
                        <Text type={'secondary'} className={c.arvisDesc} style={{ color: '#c5c5c5', lineHeight: 2.4 }}>
                            ARVIS is a Data-Powered Artificial Intelligence system that is designed to integrate and empower modern companies and C-Level management to make managerial and strategic decisions while being connected to ARVIS’s Business Intelligence Analysis, allowing them to make the best informed decisions by the visualization of Data and Automation.
                        </Text>
                    </Col>
                    <Col xs={24} md={6} >
                        <Title level={5} style={{ color: '#c5c5c5' }}>
                            Contact Us
                        </Title>
                        <Space direction={'horizontal'} size={'middle'} style={{ marginBottom: "12px" }}>
                            <Image
                                src={'/images/map.svg'}
                                width={30}
                                preview={false}
                            />
                            <Text type={'secondary'} style={{ color: '#c5c5c5', margin: '0 50px 0 0' }}>
                                JL Sunter Agung Podomoro Blok N2,
                                No. 9 - 10, Sunter, RT.10/RW.11, Sunter Jaya,
                                Tj. Priok, Kota Jakarta Utara,
                                Daerah Khusus Ibukota Jakarta 14350
                            </Text>
                        </Space>
                        <Space direction={'horizontal'} size={'middle'}>
                            <Image
                                src={'/images/email.svg'}
                                width={30}
                            />
                            <Text type={'secondary'} style={{ color: '#c5c5c5', margin: '0 50px 0 0' }}>
                                info@arvis.id
                            </Text>
                        </Space>
                    </Col>
                    <Col xs={{ span: 24, offset: 0 }} md={{ span: 5, offset: 1 }} className={c.sitemap}>
                        <Title level={5} style={{ color: '#c5c5c5' }}>
                            Sitemap
                        </Title>
                        <Space direction={'vertical'}>
                            <Button type={'link'} href={'#hero'} style={{ color: '#c5c5c5', margin: '0 50px 0 0' }}>
                                Home
                            </Button>
                            <Button type={'link'} href={'#products'} style={{ color: '#c5c5c5', margin: '0 50px 0 0' }}>
                                Products
                            </Button>
                            <Button type={'link'} href={'#values'} style={{ color: '#c5c5c5', margin: '0 50px 0 0' }}>
                                Why Us?
                            </Button>
                            <Button type={'link'} href={'#contact'} style={{ color: '#c5c5c5', margin: '0 50px 0 0' }}>
                                Contact
                            </Button>
                        </Space>
                    </Col>
                </Row>
            </div>
        )
    }

    function _renderCopyright() {
        return (
            <div style={{ padding: '0 75px', backgroundColor: '#121212', borderTop: '1px solid #272323' }}>
                <Row align={'middle'} justify={'center'} style={{ padding: '5px 0' }}>
                    <Col xs={24} md={24} style={{ textAlign: 'center', margin: '18px 0' }}>
                        <Text style={{ color: '#c5c5c5', margin: 0 }}>
                            © PT Aksara Integrasi Sejahtera, 2021
                        </Text>
                    </Col>
                </Row>
            </div>
        )
    }

    return (
        <div id={'contact'}>
            {_renderContactFooter()}
            {_renderMainFooter()}
            {_renderCopyright()}
        </div>
    )
}
