import React from "react";
import Head from "next/head";
import Hero from "../components/Homepage/Hero";
import { Navigation } from "../components/Navigation";
import Products from "../components/Homepage/Products";
import Values from "../components/Homepage/Values";
import Footer from "../components/Footer";

export default function Home(): JSX.Element {

  function _meta() {
    return (
      <Head>
        <title>Arvis - SMART ERP</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta charSet="utf-8" />
        <meta name="description" content={'meta description'}></meta>

        <meta property="og:title" content={'Arvis - SMART ERP'} key="ogtitle" />
        <meta property="og:description" content={'meta description'} key="ogdesc" />
        <link rel="icon" type="image/svg" href="/arvis-icon.svg" />
        <link
          rel="preload"
          href="/fonts/Canela/Canela-Regular.ttf"
          as="font"
          crossOrigin=""
        />
        <link
          rel="preload"
          href="/fonts/Canela/Canela-Bold.ttf"
          as="font"
          crossOrigin=""
        />
      </Head>
    )
  }


  return (
    <div>
      {_meta()}
      <Navigation />
      <div>
        <Hero />
        <Products />
        <Values />
        <Footer />
      </div>
    </div >
  );
}
